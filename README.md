# ZADANIE
System w oparciu o dowolną dystrybucję linux-a 
Provisioning stworzony w oparciu o Ansible https://www.ansible.com/
Provisioning powinien instalować serwer Jenkins https://jenkins.io/
Stworzyć/skopiować (przez ansible) prosty skrypt bash-owy który cyklicznie, co 5 minut tworzy nowe przykładowe zadanie “Job” w serwerze jenkinsa używając jenkins klienta (Jenkins CLI).
Skrypt powinien być uruchomiony jako serwis bądź dodany do crona.
Kod wynikowy należy umieścić w repozytorium kodu https://github.com/

# Wyniki

Jak uruchomic:
vagrant up

Wykona sciagniescie obrazu i vagrant provisioning, który zainstaluje wszystko co potrzeba.
Joby mozna ogladac na hoscie pod urlem (co 5 min nowy):
http://localhost:8080

Dzialanie crontaba mozna ogladac w logach ~/cron.log

Zeby zautentykowac sie nalezy zdobyc haslo dla user admin w nastepujacy sposob:
vagrant ssh
```
CONTAINER_ID=`sudo docker ps -f name=jenkins | grep jenkins | awk '{ print $1}'`
sudo docker exec $CONTAINER_ID cat /var/jenkins_home/secrets/initialAdminPassword  | tr -d '\r\n'
```

Po inicjalizacji setupu jenkinsa przez przegladarke, powinny ukazac sie nowe joby ktore dodają sie co 5 min. 

Ponizej lekturka opisująca co w jakiej kolejnosci wykonalem i z czym byly problemy. Warto poczytac. Wszelkie pytania mile widziane. 

#Krok po kroku: tok mysleniowy

- Zainstaluj Ansible na hoscie
- Zainstaluj Vagranta na hoscie

Do uruchomienia skryptu ansiblowego na starcie vagranta wystarczy zapodac do Vagrant file:
```
  #
  # Run Ansible from the Vagrant Host
  #
  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "playbook.yml"
  end
```

Jenkins: Jak go zdobyc: Pytania:
Czy istnieje docker dla jenkinsa?
https://github.com/jenkinsci/docker/blob/master/README.md
Klasyczny przypadek uruchomienia to:
```
docker run -p 8080:8080 -p 50000:50000 jenkins/jenkins:lts
```
Wiec potrzebujemy 2 porty (na ten moment nie rozpatrujemy volumeny)

Mamy vagranta i jenkinsa w dockerze. Teraz jak uruchomic dockera w obrazie vagranta.
Po drodze natrafilem na 
https://www.vagrantup.com/docs/provisioning/docker.html
w skrócie: "The Vagrant Docker provisioner can automatically install Docker, pull Docker containers, and configure certain containers to run on boot."
To cos idealnie pasuje do zainstalowania dockera i zaciagniecia kontenera jenkinsa i uruchomienia go na starcie. 

Wiem ze zadaniem było uzycie ansible a nie docker provisioning, wykonałem obie sciezki:
- instalacja przez docker provisioning (prostrzy wariant)
- instalacja przez Ansible (zgodnie z trescią zadania, znajduje sie w sekcji nizej)

## Wariant 1: DOCKER PROVISIONING
Testujemy opcje:
```
  config.vm.provision "docker" do |d|
    d.run "jenkins/jenkins"
  end
```
Uruchamiamy:
```
jacek@jacek-desktop ~/vagrant/xenial $ vagrant ssh

jacek@jacek-desktop ~/vagrant/xenial $ vagrant provision
==> default: Running provisioner: docker...
    default: Installing Docker onto machine...
==> default: Starting Docker containers...
==> default: -- Container: jenkins/jenkins
```
Po wejsciu przez vagrant ssh mozemy sprawdzic:
```
vagrant@ubuntu-xenial:~$ ps aux | grep jenkins
vagrant   4631  0.0  0.0   1148     4 ?        Ss   19:41   0:00 /sbin/tini -- /usr/local/bin/jenkins.sh
vagrant   4667 24.1 22.7 2773272 230776 ?      Sl   19:41   0:14 java -Duser.home=/var/jenkins_home -jar /usr/share/jenkins/jenkins.war
vagrant   4818  0.0  0.0  12916  1012 pts/0    S+   19:42   0:00 grep --color=auto jenkins
```
Jenkins zainstalowany i uruchomiony na starcie
```
vagrant@ubuntu-xenial:~$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                 NAMES
266317d77d6a        jenkins/jenkins     "/sbin/tini -- /usr/…"   2 minutes ago       Up 2 minutes        8080/tcp, 50000/tcp   jenkins-jenkins

vagrant@ubuntu-xenial:~$ curl http://localhost:8080
curl: (7) Failed to connect to localhost port 8080: Connection refused
```
Curl sie nie udał bo nie "wyexpozowalimsy" portu 8080

Spróbujemy recznie: 
```
vagrant@ubuntu-xenial:~$ docker run -d -p 8080:8080 -p 50000:50000 jenkins

vagrant@ubuntu-xenial:~$ docker ps | grep jenkins
4d2a79496109        jenkins             "/bin/tini -- /usr/l…"   10 seconds ago      Up 9 seconds        0.0.0.0:8080->8080/tcp, 0.0.0.0:50000->50000/tcp   unruffled_khorana
vagrant@ubuntu-xenial:~$ curl http://localhost:8080
<html><head><meta http-equiv='refresh' content='1;url=/login?from=%2F'/><script>window.location.replace('/login?from=%2F');</script></head><body style='background-color:white; color:white;'>
Authentication required
<!--
You are authenticated as: anonymous
Groups that you are in:
  
Permission you need to have (but didn't): hudson.model.Hudson.Administer
-->

</body></html>
```
Jenkins dziala. Wiec trzeba teraz zupdatowac skrypt uruchomieniowy dla Vagranta o dodatkowe porty jako arguementy
Aktualizujemy do:
```
  config.vm.provision "docker" do |d|
    d.run "jenkins/jenkins",
      args: "-p 8080:8080 -p 50000:50000"
  end
```
Ponowne uruchomienie wykryło zmiane argumentow:
```
jacek@jacek-desktop ~/vagrant/xenial $ vagrant provision
==> default: Running provisioner: docker...
==> default: Starting Docker containers...
==> default: -- Container: jenkins/jenkins
==> default: -- Detected changes to container 'jenkins/jenkins' args, restarting..

vagrant@ubuntu-xenial:~$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                              NAMES
14192e52aabd        jenkins/jenkins     "/sbin/tini -- /usr/…"   12 seconds ago      Up 12 seconds       0.0.0.0:8080->8080/tcp, 0.0.0.0:50000->50000/tcp   jenkins-jenkins
```
Juz mamy przekierowanie z vagranta do jenkinsa na dockerze i
```
vagrant@ubuntu-xenial:~$ curl http://localhost:8080
```
Curl dziala.

Teraz chcielibysmy miec dostep do dashboarda jenkinsa z poziomu maszyny hosta
Wykozystujemy forward port dla vagranta:
```
  config.vm.network "forwarded_port", guest: 8080, host: 8080
```
Po restacie na hoscie mamy:
jacek@jacek-desktop ~/vagrant/xenial $ curl http:/localhost:8080
I ekran z komunikatem: To ensure Jenkins is securely set up by the administrator, a password has been written to the log (not sure where to find it?) and this file on the server: 

Wyciagamy te dane. 
Trzeba dostac sie do vagranta, a potem do kontenera dockera
```
vagrant@ubuntu-xenial:~$ docker exec -i -t a123df7e24af /bin/bash

jenkins@df56639584b3:/$ cat /var/jenkins_home/secrets/initialAdminPassword
6c6d9cfa9df64673831988d5c11c6a48
```
Dostalem sie do jenkinsa.

## Wariant 2: ANSIBLE PROVISIONING

Do odpalenia ansibla potrzebowalem postudiowac nieco ansible. 
Zabralem sie od razu Vagranta z provisioningiem:
```
  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "playbook.yml"
  end
```
i playbookiem z dockerem. Jest to dodatkowy plugin który uzywa docker_py.
Tutaj spedzilem dosc sporo czasu na róznego typu przygodach z samym pythonem, pluginem docker_container i docker_py. 
Pasmo problemow z wersjami pytona, docker_py. Natrafilem na wrecz komiczne bugi plugina docker_py, np:
Error: docker-py version is 1.10.6. Minimum version required is 1.7.0."
Prosili zeby downgradowac do nizszej wersji. 
Generalnie to komedia, jesli chodzi o ilosc bugow i problemow jakie napodkalem. Cale szczescie juz po. Wszystkie solved. 

Uzylem ansiblowego taska z dostepnego po zainstalowaniu pluginu:
```
  - name: Run docker container
    docker_container:
      name: jenkins
      image: jenkins/jenkins
      ports: 
      - "8080:8080"
      - "50000:50000"
```
Efekt koncowy to uruchomiony jenkins w wagrancie w dockerze i przeforwardowane porty do hosta. 

# JENKINS CLI jak uzyc

Zabieramy sie za jenkins-cli
Po przestudiowaniu troche dokumentacji wiemy ze trza sciagnac jara wiec robimy:
```
curl http://localhost:8080/jnlpJars/jenkins-cli.jar -o jenkins-cli.jar
```
wiec potrzebujemy na wirtualce jave. Musimy rozszerzyc skrypt playbook.yml o instalacje javy

A teraz jak sie zautentykowac jenkins-cli'entem?
Trzeba wyciagnac auth token generowany do pliku z obrazu dockera. 
Najpierw trza sie do niego dobrac. Wyciagamy ID kontenera:
```
CONTAINER_ID=`sudo docker ps -f name=jenkins | grep jenkins | awk '{ print $1}'`
```
Potem:
```
AUTH=admin:`sudo docker exec -i -t $CONTAINER_ID cat /var/jenkins_home/secrets/initialAdminPassword  | tr -d '\r\n'`

vagrant@ubuntu-xenial:~$ java -jar jenkins-cli.jar -s http://localhost:8080 -auth $AUTH who-am-i
Authenticated as: admin
Authorities:
  authenticated
```

Budujemy sobie przykladowy job i exportujemy go:
```
java -jar jenkins-cli.jar -s http://localhost:8080 -auth $AUTH get-job test > myNewJob.xml
```
Potem importujemy jako nowy job:
```
java -jar jenkins-cli.jar -s http://localhost:8080 -auth $AUTH create-job newJob1 < myNewJob.xml
```
Kompletujemy skrypt i w wyniku mamy:
addJenkinsJob.sh (patrz katalog obok)
 
# kopiujemy pliki przez ansible
Proste zadanie, nie potrzeba komentarza. Zwykla komenda copy w ansiblu

# dodajemy wpis do crona
Prosty wpis do playboka. Nie wymaga komentarza




