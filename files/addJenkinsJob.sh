#!/bin/bash

JENKINS_JAR=jenkins-cli.jar

echo "[JENKINS] script for add new jenkins job"

if [ ! -f $JENKINS_JAR ]; then
   echo "[JENKINS] File $JENKINS_JAR does not exist. Downloading jenkins client jar file."
   curl http://localhost:8080/jnlpJars/$JENKINS_JAR -o $JENKINS_JAR
fi

# Get docker container ID
CONTAINER_ID=`sudo docker ps -f name=jenkins | grep jenkins | awk '{ print $1}'`

echo "[JENKINS] ContainerId:" $CONTAINER_ID

# Get auth token for admin user
AUTH=admin:`sudo docker exec $CONTAINER_ID cat /var/jenkins_home/secrets/initialAdminPassword  | tr -d '\r\n'`

echo "[JENKINS] Auth:" $AUTH

# Create new job
java -jar $JENKINS_JAR -s http://localhost:8080 -auth $AUTH create-job newJob$((RANDOM % 10000)) < myNewJob.xml

echo "[JENKINS] Script execution finished"
